#ФАЙЛ ДЛЯ РЕГИСТРАЦИИ МОДЕЛЕЙ
from django.contrib import admin
from apps.catalog.models import *  #Импорт из модели *-импорт всех моделей

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)} # автозаполнение слага (урл)
    list_display = ['title', 'slug', 'publish', ] # отображение полей

@admin.register(Collection)
class CollectionAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = ['title', 'slug', 'publish', ]


@admin.register(Material)
class MaterialAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = ['title', 'slug', 'publish', ]


@admin.register(Color)
class ColorAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = ['title', 'slug', 'publish', ]

@admin.register(PriceType)
class PriceTypeAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = ['title', 'slug', 'publish', ]


class PriceInline(admin.TabularInline):
    model = Price
    extra = 0

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = ['title', 'slug', 'publish', ]
    filter_horizontal = ['colors', ]
    inlines = [PriceInline, ]